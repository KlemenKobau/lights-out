/**
 * This class represents the backend ProblemDTO
 */
export class Problem {
  id: number;
  initialState: boolean[][];
}
