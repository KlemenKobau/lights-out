/**
 * This class represents the backend Point
 */
export class Point {
  xCoordinate: number;
  yCoordinate: number;
}
