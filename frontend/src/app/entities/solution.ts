import {Point} from './point';

/**
 * Represents the backend SolutionDTO
 */
export class Solution {
  clickPoints: Point[];
}
