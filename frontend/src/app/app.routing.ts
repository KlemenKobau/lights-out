import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {NotFoundComponent} from "./components/not-found/not-found.component";
import {GameProblemComponent} from "./components/game-problem/game-problem.component";
import {GameSubmitComponent} from "./components/game-submit/game-submit.component";
import {GamePlayComponent} from "./components/game-play/game-play.component";

const routes: Routes = [
  {path: 'play', component: GameProblemComponent, pathMatch: 'full'},
  {path: 'play/:problemId', component: GamePlayComponent},
  {path: 'submit', component: GameSubmitComponent},
  {path: '', redirectTo: 'play', pathMatch: 'full'},
  {path: '**', component: NotFoundComponent}
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
