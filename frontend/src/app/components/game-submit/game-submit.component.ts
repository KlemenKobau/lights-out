import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import {ProblemService} from '../../services/problem.service';
import {Problem} from '../../entities/problem';
import {ClickMode, GameSolutionCardComponent} from '../building-blocks/game-solution-card/game-solution-card.component';
import {Subscription} from 'rxjs';
import {MatSnackBar} from '@angular/material/snack-bar';

/**
 * Submit problems
 */
@Component({
  selector: 'app-game-submit',
  templateUrl: './game-submit.component.html',
  styleUrls: ['./game-submit.component.scss']
})
export class GameSubmitComponent implements OnInit, OnDestroy {

  private subscriptions: Subscription[] = [];
  private snackbarDuration = 3 * 1000;

  /**
   * The game board
   */
  @ViewChild(GameSolutionCardComponent) gameHolder: GameSolutionCardComponent;

  clickMode = ClickMode.SINGLE;

  /**
   * The board side
   */
  dimension = 4;

  /**
   * Settings for the game board
   */
  dimensionFormControl: FormControl;

  constructor(private problemService: ProblemService, private snackBar: MatSnackBar) {
  }

  ngOnInit(): void {
    // create the form controls
    this.dimensionFormControl = new FormControl(4,
      [Validators.required, Validators.min(3), Validators.max(8)]);

    // regenerate the board on dimension changes
    this.subscriptions.push(this.dimensionFormControl.valueChanges.subscribe(value => {
      if (this.dimensionFormControl.valid) {
        this.dimension = value;
      }
    }));
  }

  /**
   * Called when the submit button is clicked
   */
  onSubmitClick(): void {
    const tiles = this.gameHolder.tiles;

    // create an object the database can use
    const problem = new Problem();
    const initialState = [];

    for (let i = 0; i < this.dimension; i++) {
      const row = [];
      for (let j = 0; j < this.dimension; j++) {
        row.push(tiles[i][j].isNotPressed); // false represents a light
      }
      initialState.push(row);
    }

    problem.initialState = initialState;

    // post the problem
    this.subscriptions.push(this.problemService.addNewProblem(problem).subscribe(() => {
      this.openSnackBar('Problem submitted successfully');
    }, error => this.openSnackBar(error.error.message)));
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(value => value.unsubscribe());
  }

  /**
   * Called when the reset button is clicked
   */
  onResetClick(): void {
    this.gameHolder.resetField();
  }

  /**
   * Helper function for opening the snack bar
   * @param message the message to display
   */
  private openSnackBar(message: string): void {
    this.snackBar.open(message, 'close', {
      duration: this.snackbarDuration,
      horizontalPosition: 'end', verticalPosition: 'top'
    });
  }
}
