import {ComponentFixture, TestBed} from '@angular/core/testing';

import {GameSubmitComponent} from './game-submit.component';

describe('GameSubmitComponent', () => {
  let component: GameSubmitComponent;
  let fixture: ComponentFixture<GameSubmitComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [GameSubmitComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GameSubmitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
