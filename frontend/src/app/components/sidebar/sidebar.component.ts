import {Component} from '@angular/core';
import {Router} from '@angular/router';

/**
 *
 */
@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent {

  constructor(private router: Router) {
  }

  /**
   * Called when the problem button is clicked in the sidebar
   */
  onProblemClick(): void {
    this.router.navigate(['/play']);
  }

  /**
   * Called when the button to submit is clicked in the sidebar
   */
  onProblemSubmitClick(): void {
    this.router.navigate(['/submit']);
  }
}
