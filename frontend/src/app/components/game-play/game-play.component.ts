import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ProblemService} from '../../services/problem.service';
import {Subscription} from 'rxjs';
import {Problem} from '../../entities/problem';
import {ClickMode, GameSolutionCardComponent} from '../building-blocks/game-solution-card/game-solution-card.component';
import {SolutionService} from '../../services/solution.service';
import {Solution} from '../../entities/solution';

/**
 * User plays the game
 */
@Component({
  selector: 'app-game-play',
  templateUrl: './game-play.component.html',
  styleUrls: ['./game-play.component.scss']
})
export class GamePlayComponent implements OnInit, OnDestroy {

  /**
   * The game board
   */
  @ViewChild(GameSolutionCardComponent) gameHolder: GameSolutionCardComponent;
  /**
   * If we have solved the problem or not
   */
  solved = false;
  readonly clickMode = ClickMode.CROSS;
  /**
   * The problem we are solving
   */
  problem: Problem;
  /**
   * The solution to the problem we are solving
   */
  solution: Solution;
  private subscriptions: Subscription[] = [];

  constructor(private activatedRoute: ActivatedRoute,
              private problemService: ProblemService,
              private solutionService: SolutionService,
              private router: Router) {
  }

  ngOnInit(): void {
    this.subscriptions.push(this.activatedRoute.paramMap.subscribe(value => {

      // if the id is not a number, reroute to the start page
      const problemId = Number(value.get('problemId').valueOf());
      if (isNaN(problemId)) {
        this.router.navigate(['/play']);
      }

      // when we have the id, query the problem, but unsubscribe when done
      const getDataSubscription = this.problemService.getProblemById(problemId).subscribe(problem => {
        this.problem = problem;
        getDataSubscription.unsubscribe();
      });
    }));
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(value => value.unsubscribe());
  }

  /**
   * Call when the solved observable triggers
   */
  onProblemSolved(): void {
    this.solved = true;
  }

  /**
   * Called when the show solution button is clicked
   */
  onShowSolutionClick(): void {
    this.subscriptions.push(this.solutionService.getSolutionsById(this.problem.id)
      .subscribe(value => this.solution = value[0]));
  }

  /**
   * Called when the reset button is clicked
   */
  onResetClick(): void {
    this.gameHolder.resetField();
  }
}
