import {Component, Input, OnChanges, Output} from '@angular/core';
import {flatten} from '@angular/compiler';
import {Subject} from 'rxjs';
import {Solution} from '../../../entities/solution';

/**
 * Represents a single game grid
 */
@Component({
  selector: 'app-game-solution-card',
  templateUrl: './game-solution-card.component.html',
  styleUrls: ['./game-solution-card.component.scss']
})
export class GameSolutionCardComponent implements OnChanges {

  /**
   * The tiles that are not pressed.
   * At the end we want to have only ones on the whole grid.
   */
  @Input()
  notPressedTiles: boolean[][];

  /**
   * The length of a side of the side matrix.
   */
  @Input()
  dimension: number;

  /**
   * How clicks should be handled
   */
  @Input()
  clickMode = ClickMode.DISABLED;

  /**
   * The solution to be overlaid over the grid
   */
  @Input()
  solution: Solution;

  /**
   * Outputs void when a problem is solved
   */
  @Output()
  problemSolvedObservable = new Subject();

  // the objects in tiles in flatTiles are the same, changes will be reflected in both
  /**
   * The inner representation of the game board
   */
  tiles: Tile[][];

  /**
   * The inner representation of the game board in vector form
   */
  flatTiles: Tile[];

  constructor() {
  }

  ngOnChanges(): void {
    // create the problem
    if (this.notPressedTiles) {
      this.dimension = this.notPressedTiles.length;
    }

    if (this.dimension) {
      this.tiles = [];

      for (let i = 0; i < this.dimension; i++) {
        const row = [];
        for (let j = 0; j < this.dimension; j++) {
          const tile = new Tile();
          tile.x = j;
          tile.y = i;
          tile.isInSolution = false;

          // default is true (no light)
          tile.isNotPressed = !(this.notPressedTiles && !this.notPressedTiles[i][j]);
          row.push(tile);
        }
        this.tiles.push(row);
      }

      this.flatTiles = flatten(this.tiles);
      this.checkIfSolved();
    }

    // display solution
    if (this.solution) {
      for (const clickPoint of this.solution.clickPoints) {
        this.tiles[clickPoint.yCoordinate][clickPoint.xCoordinate].isInSolution = true;
      }
    }
  }

  /**
   * Handle tile clicks
   * @param clickedTile the clicked tile
   */
  onTileClick(clickedTile: Tile): void {
    if (this.clickMode === ClickMode.DISABLED) {
      return;
    } else if (this.clickMode === ClickMode.SINGLE || this.clickMode === ClickMode.CROSS) {
      clickedTile.isNotPressed = !clickedTile.isNotPressed;
    }

    if (this.clickMode === ClickMode.CROSS) {
      // click neighbour tiles

      if (clickedTile.x !== 0) {
        // click left
        this.tiles[clickedTile.y][clickedTile.x - 1].isNotPressed = !this.tiles[clickedTile.y][clickedTile.x - 1].isNotPressed;
      }
      if (clickedTile.x !== this.dimension - 1) {
        // click right
        this.tiles[clickedTile.y][clickedTile.x + 1].isNotPressed = !this.tiles[clickedTile.y][clickedTile.x + 1].isNotPressed;
      }
      if (clickedTile.y !== 0) {
        // click right
        this.tiles[clickedTile.y - 1][clickedTile.x].isNotPressed = !this.tiles[clickedTile.y - 1][clickedTile.x].isNotPressed;
      }
      if (clickedTile.y !== this.dimension - 1) {
        // click right
        this.tiles[clickedTile.y + 1][clickedTile.x].isNotPressed = !this.tiles[clickedTile.y + 1][clickedTile.x].isNotPressed;
      }
    }

    this.checkIfSolved();
  }

  /**
   * Reset the field
   */
  resetField(): void {
    if (this.notPressedTiles) {
      for (let i = 0; i < this.dimension; i++) {
        for (let j = 0; j < this.dimension; j++) {
          this.tiles[i][j].isNotPressed = this.notPressedTiles[i][j];
        }
      }
    } else {
      for (const tile of this.flatTiles) {
        tile.isNotPressed = true;
      }
    }
  }

  private checkIfSolved(): void {
    if (this.problemSolvedObservable.observers.length === 0) {
      return;
    }

    for (const tile of this.flatTiles) {
      if (!tile.isNotPressed) {
        return;
      }
    }

    // the problem is solved, disable clicks and notify observers
    this.clickMode = ClickMode.DISABLED;
    this.problemSolvedObservable.next();
  }
}

class Tile {
  isNotPressed: boolean;
  x: number;
  y: number;
  isInSolution: boolean;
}

export enum ClickMode {
  SINGLE = 'SINGLE', CROSS = 'CROSS', DISABLED = 'DISABLED'
}
