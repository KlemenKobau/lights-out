import {ComponentFixture, TestBed} from '@angular/core/testing';

import {GameSolutionCardComponent} from './game-solution-card.component';

describe('GameSolutionCardComponent', () => {
  let component: GameSolutionCardComponent;
  let fixture: ComponentFixture<GameSolutionCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [GameSolutionCardComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GameSolutionCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
