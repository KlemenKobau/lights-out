import {Component} from '@angular/core';

/**
 * The generic component for 404 page calls
 */
@Component({
  selector: 'app-not-found',
  templateUrl: './not-found.component.html',
  styleUrls: ['./not-found.component.scss']
})
export class NotFoundComponent {

  constructor() {
  }

}
