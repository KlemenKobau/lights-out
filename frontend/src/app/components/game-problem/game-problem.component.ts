import {Component, OnDestroy, OnInit} from '@angular/core';
import {ProblemService} from '../../services/problem.service';
import {Problem} from '../../entities/problem';
import {Router} from '@angular/router';
import {Subscription} from 'rxjs';

/**
 * The gallery of game problems
 */
@Component({
  selector: 'app-game-problem',
  templateUrl: './game-problem.component.html',
  styleUrls: ['./game-problem.component.scss']
})
export class GameProblemComponent implements OnInit, OnDestroy {

  private subscriptions: Subscription[] = [];

  /**
   * The problem gallery
   */
  problems: Problem[];

  constructor(private problemService: ProblemService, private router: Router) {
  }

  ngOnInit(): void {
    this.subscriptions.push(this.problemService.getAllProblems().subscribe(value => this.problems = value));
  }

  /**
   * Called when a problem is clicked
   * @param problem the clicked problem
   */
  onCardClick(problem: Problem): void {
    this.router.navigate(['/play', problem.id]);
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(value => value.unsubscribe());
  }
}
