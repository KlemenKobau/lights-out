import {ComponentFixture, TestBed} from '@angular/core/testing';

import {GameProblemComponent} from './game-problem.component';

describe('GameProblemComponent', () => {
  let component: GameProblemComponent;
  let fixture: ComponentFixture<GameProblemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [GameProblemComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GameProblemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
