import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {SidebarComponent} from './components/sidebar/sidebar.component';
import {MatButtonModule} from '@angular/material/button';
import {AppRoutingModule} from "./app.routing";
import {GameProblemComponent} from './components/game-problem/game-problem.component';
import {NotFoundComponent} from './components/not-found/not-found.component';
import {GameSubmitComponent} from './components/game-submit/game-submit.component';
import {GameSolutionCardComponent} from './components/building-blocks/game-solution-card/game-solution-card.component';
import {MatCardModule} from "@angular/material/card";
import {MatGridListModule} from "@angular/material/grid-list";
import {MatInputModule} from '@angular/material/input';
import {ReactiveFormsModule} from "@angular/forms";
import {HttpClientModule} from '@angular/common/http';
import {GamePlayComponent} from './components/game-play/game-play.component';
import {MatSnackBarModule} from '@angular/material/snack-bar';

@NgModule({
  declarations: [
    AppComponent,
    SidebarComponent,
    GameProblemComponent,
    NotFoundComponent,
    GameSubmitComponent,
    GameSolutionCardComponent,
    GamePlayComponent,
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatCardModule,
    MatGridListModule,
    MatInputModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatSnackBarModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
