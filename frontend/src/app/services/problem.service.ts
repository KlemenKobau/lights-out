import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Problem} from '../entities/problem';
import {Observable} from 'rxjs';

/**
 * CRUD operations for problems
 */
@Injectable({
  providedIn: 'root'
})
export class ProblemService {

  private readonly PROBLEM_ENDPOINT = '/problems';

  constructor(private http: HttpClient) {
  }

  /**
   * Get all problems from the database
   */
  getAllProblems(): Observable<Problem[]> {
    return this.http.get<Problem[]>(environment.backendUrl + this.PROBLEM_ENDPOINT);
  }

  /**
   * Get a problem from the database by id
   * @param problemId the id of the problem
   */
  getProblemById(problemId: number): Observable<Problem> {
    return this.http.get<Problem>(environment.backendUrl + this.PROBLEM_ENDPOINT + '/' + problemId);
  }

  /**
   * Send a problem to the database to save it.
   * It is only saved if a solution exists.
   * @param problem the problem to be saved
   */
  addNewProblem(problem: Problem): Observable<any> {
    return this.http.post(environment.backendUrl + this.PROBLEM_ENDPOINT, problem);
  }
}
