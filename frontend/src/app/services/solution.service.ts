import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Solution} from '../entities/solution';
import {Observable} from 'rxjs';

/**
 * CRUD operation for Solutions
 */
@Injectable({
  providedIn: 'root'
})
export class SolutionService {

  private readonly SOLUTION_ENDPOINT = '/solutions';

  constructor(private http: HttpClient) {
  }

  /**
   * Get solutions by id
   * @param problemId the id of the problem we are interested in
   */
  getSolutionsById(problemId: number): Observable<Solution[]> {
    return this.http.get<Solution[]>(environment.backendUrl + this.SOLUTION_ENDPOINT + '/problem/' + problemId);
  }
}
