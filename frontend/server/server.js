const express = require('express');
const http = require('http');
const path = require('path');

const httpPort = process.env['HTTP_PORT'] || 4200;
const webAppPath = process.env['WEBAPP_PATH'] || '../dist/frontend';

const app = express();

const appDistFolder = path.join(process.cwd(), webAppPath);

// Serve static files from /browser
app.use('/', express.static(appDistFolder));

app.get('*', (req, res) => {
  res.sendFile(path.join(appDistFolder + '/index.html'));
});

// Start up the Node server
http.createServer(app).listen(httpPort, function () {
  console.info('Listening for HTTP requests on port ' + httpPort);
});
