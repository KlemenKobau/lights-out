cd database || exit
echo 'Building database'
./mvnw clean package
cd ..

cd frontend || exit
echo 'Installing frontend dependencies'
npm install
echo 'Building frontend'
./node_modules/@angular/cli/bin/ng b --prod
echo 'Finished building frontend'
cd ..

echo 'Setting everything up in docker'
docker-compose up -d
