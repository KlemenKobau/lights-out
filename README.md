# Lights Out
This project contains the frontend and the backend for the game Lights out.

The project can be started in docker by running the **setup.sh** file located in the root directory.
The following commands are needed for this:
- npm
- docker
- docker-compose

## Frontend
The frontend uses Angular and is accessible from http://localhost:4200, when running.
A user can submit problems to the backend, which solves them and notifies the user about the success.
When a solution is found, the problem is persisted to an in-memory database.
For more information, look at the README in the frontend folder.

## Backend
An openapi UI is exposed at http://localhost:8080/q/swagger-ui/.
The backend uses an in-memory database to save the problems, so the problems get deleted at each 
restart.
For more information look at the README in the database folder.
