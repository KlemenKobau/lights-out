package com.lights_out.dto;

/**
 * A dto representing the lights out problem
 */
public class ProblemDTO {
    private Long id;
    private boolean[][] initialState;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Get the problem representation
     *
     * @return a boolean array representing the problem
     */
    public boolean[][] getInitialState() {
        return initialState;
    }

    /**
     * Set the problem representation
     *
     * @param initialState the problem representation
     */
    public void setInitialState(boolean[][] initialState) {
        this.initialState = initialState;
    }
}
