package com.lights_out.dto;

/**
 * A wrapper class for a problem with a solution
 */
public class ProblemWithSolutionDTO {
    private ProblemDTO problem;
    private SolutionDTO solution;

    public ProblemDTO getProblem() {
        return problem;
    }

    public void setProblem(ProblemDTO problem) {
        this.problem = problem;
    }

    public SolutionDTO getSolution() {
        return solution;
    }

    public void setSolution(SolutionDTO solution) {
        this.solution = solution;
    }
}
