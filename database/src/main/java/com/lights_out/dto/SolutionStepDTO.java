package com.lights_out.dto;

import com.lights_out.entities.game.Point;

/**
 * A dto class representing a step in the solution
 */
public class SolutionStepDTO {
    private Long solutionFK;
    private Point clickPoint;
    private Integer stepIndex;

    /**
     * Get the id of the associated solution
     *
     * @return the id of the solution
     */
    public Long getSolutionFK() {
        return solutionFK;
    }

    /**
     * Set the id of the associated solution
     *
     * @param solutionFK the id of the solution
     */
    public void setSolutionFK(Long solutionFK) {
        this.solutionFK = solutionFK;
    }

    /**
     * Get the point click preformed in this step
     *
     * @return the point click
     */
    public Point getClickPoint() {
        return clickPoint;
    }

    /**
     * Set the point click preformed in this step
     *
     * @param clickPoint the point click
     */
    public void setClickPoint(Point clickPoint) {
        this.clickPoint = clickPoint;
    }

    /**
     * Get the index at which this step is performed in the associated solution
     *
     * @return the index of the step
     */
    public Integer getStepIndex() {
        return stepIndex;
    }

    /**
     * Set the index at which this step is performed in the associated solution
     *
     * @param stepIndex the index of the step
     */
    public void setStepIndex(Integer stepIndex) {
        this.stepIndex = stepIndex;
    }
}
