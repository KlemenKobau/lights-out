package com.lights_out.dto;

import com.lights_out.entities.game.Point;

import java.util.List;

/**
 * DTO class representing a solution
 */
public class SolutionDTO {
    private Long problemFK;
    private List<Point> clickPoints;

    /**
     * Get the id of the problem associated to this solution
     *
     * @return the id of the problem
     */
    public Long getProblemFK() {
        return problemFK;
    }

    /**
     * Set the id of the problem associated to this solution
     *
     * @param problemFK the id of the problem
     */
    public void setProblemFK(long problemFK) {
        this.problemFK = problemFK;
    }

    /**
     * Get a list of points representing this sollution
     *
     * @return a list of points
     */
    public List<Point> getClickPoints() {
        return clickPoints;
    }

    /**
     * Set a list of points representing this solution
     *
     * @param clickPoints a list of points
     */
    public void setClickPoints(List<Point> clickPoints) {
        this.clickPoints = clickPoints;
    }
}
