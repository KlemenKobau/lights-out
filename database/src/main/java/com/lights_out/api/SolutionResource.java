package com.lights_out.api;

import com.lights_out.dto.ProblemWithSolutionDTO;
import com.lights_out.dto.SolutionDTO;
import com.lights_out.services.SolutionBean;
import org.eclipse.microprofile.openapi.annotations.enums.SchemaType;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponses;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Exposes endpoints for solutions
 */
@Tag(name = "solutions")
@Path("/solutions")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class SolutionResource {

    private final SolutionBean solutionBean;

    @Inject
    public SolutionResource(SolutionBean solutionBean) {
        this.solutionBean = solutionBean;
    }

    /**
     * An endpoint for getting all solutions
     *
     * @return A response containing a list of all available solutions
     */
    @APIResponse(responseCode = "200", description = "All solutions",
            content = @Content(schema =
            @Schema(type = SchemaType.ARRAY, implementation = ProblemWithSolutionDTO.class)))
    @GET
    public Response getAllSolutions() {
        return Response.ok(solutionBean.getAllSolutions()).build();
    }

    /**
     * An endpoint for getting solutions for a specific problem
     *
     * @param id the id of the problem whose solutions we are looking for
     * @return a response containing all saved solutions for a problem or an empty response in case of errors
     */
    @APIResponses({
            @APIResponse(responseCode = "200", description = "All solutions for a problem",
                    content = @Content(schema =
                    @Schema(implementation = SolutionDTO.class))),
            @APIResponse(responseCode = "400", description = "Problem with provided id not found")
    })
    @GET
    @Path("/problem/{id}")
    public Response getAllSolutionsForProblem(@PathParam("id") long id) {
        return Response.ok(solutionBean.getAllSolutionsForProblem(id)).build();
    }

}
