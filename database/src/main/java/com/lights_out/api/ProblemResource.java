package com.lights_out.api;


import com.lights_out.dto.ProblemDTO;
import com.lights_out.services.ProblemBean;
import org.eclipse.microprofile.openapi.annotations.enums.SchemaType;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponses;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Optional;

/**
 * Exposes REST endpoints for problems
 */
@Tag(name = "problems")
@Path("/problems")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ProblemResource {

    private final ProblemBean problemBean;

    @Inject
    public ProblemResource(ProblemBean problemBean) {
        this.problemBean = problemBean;
    }

    /**
     * An endpoint for getting all registered problems
     *
     * @return a response with a list of problems
     */
    @APIResponse(responseCode = "200", description = "All problems",
            content = @Content(schema =
            @Schema(type = SchemaType.ARRAY, implementation = ProblemDTO.class)))
    @GET
    public Response getAllProblems() {
        return Response.ok(problemBean.getAllProblems()).build();
    }

    /**
     * An endpoint for getting a problem by id
     *
     * @param id the id of the requested problem
     * @return a response with the requested problem or an empty response in case of an error
     */
    @APIResponses({
            @APIResponse(responseCode = "200", description = "A problem by id",
                    content = @Content(schema =
                    @Schema(implementation = ProblemDTO.class))),
            @APIResponse(responseCode = "400", description = "Malformed id"),
            @APIResponse(responseCode = "404", description = "Problem not found")
    })
    @GET
    @Path("/{id}")
    public Response getProblemById(@PathParam("id") Long id) {
        if (id == null) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        Optional<ProblemDTO> problemDTOOptional = problemBean.getProblemById(id);
        if (problemDTOOptional.isEmpty()) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        return Response.ok(problemDTOOptional.get()).build();
    }

    /**
     * An endpoint for posting new problems
     *
     * @param dto A problem we wish to save
     * @return An okay response in case of success otherwise throws an exception
     */
    @APIResponses({
            @APIResponse(responseCode = "200", description = "Problem saved"),
            @APIResponse(responseCode = "400", description = "The provided problem has no solution" +
                    " or does not have a correct size")
    })
    @POST
    public Response addProblem(ProblemDTO dto) {
        problemBean.saveAProblem(dto);
        return Response.ok().build();
    }
}
