package com.lights_out.entities;

import com.lights_out.dto.SolutionDTO;

/**
 * An entity representing a solution
 */
public class Solution extends BaseEntity {
    private Long problemFK;

    public Solution() {
    }

    public Solution(SolutionDTO dto) {
        this.problemFK = dto.getProblemFK();
    }

    public Solution(Long problemFK) {
        this.problemFK = problemFK;
    }

    /**
     * Get the id of the associated problem
     *
     * @return the problem id
     */
    public Long getProblemFK() {
        return problemFK;
    }

    /**
     * Set the id of the associated problem
     *
     * @param problemFK the problem id
     */
    public void setProblemFK(Long problemFK) {
        this.problemFK = problemFK;
    }
}
