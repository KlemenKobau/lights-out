package com.lights_out.entities;

import com.lights_out.dto.ProblemDTO;

/**
 * Entity class of the problem
 */
public class Problem extends BaseEntity {
    private boolean[][] initialState;

    public Problem(){}

    public Problem(ProblemDTO problemDTO) {
        this.initialState = problemDTO.getInitialState();
        this.setId(problemDTO.getId());
    }

    public Problem(boolean[][] initialState) {
        this.initialState = initialState;
    }

    /**
     * Transform to a dto representation
     *
     * @return a dto
     */
    public ProblemDTO toProblemDTO() {
        ProblemDTO out = new ProblemDTO();
        out.setId(this.getId());
        out.setInitialState(this.getInitialState());
        return out;
    }

    /**
     * Get the initial state representing this problem
     *
     * @return the initial state
     */
    public boolean[][] getInitialState() {
        return initialState;
    }

    /**
     * Set the initial state representing this problem
     *
     * @param initialState the initial state
     */
    public void setInitialState(boolean[][] initialState) {
        this.initialState = initialState;
    }
}
