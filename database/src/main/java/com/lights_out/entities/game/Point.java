package com.lights_out.entities.game;

import java.util.Objects;

/**
 * A class representing a point in 2D space
 */
public class Point {

    // with regards to a T[][] array
    // the first dimension is y and the second is x
    private int yCoordinate;
    private int xCoordinate;

    public Point() {
    }

    public Point(int yCoordinate, int xCoordinate) {
        this.yCoordinate = yCoordinate;
        this.xCoordinate = xCoordinate;
    }

    public int getyCoordinate() {
        return yCoordinate;
    }

    public void setyCoordinate(int yCoordinate) {
        this.yCoordinate = yCoordinate;
    }

    public int getxCoordinate() {
        return xCoordinate;
    }

    public void setxCoordinate(int xCoordinate) {
        this.xCoordinate = xCoordinate;
    }

    @Override
    public String toString() {
        return "Point{" +
                "yCoordinate=" + yCoordinate +
                ", xCoordinate=" + xCoordinate +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Point point = (Point) o;
        return yCoordinate == point.yCoordinate && xCoordinate == point.xCoordinate;
    }

    @Override
    public int hashCode() {
        return Objects.hash(yCoordinate, xCoordinate);
    }
}
