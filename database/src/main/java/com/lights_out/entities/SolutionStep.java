package com.lights_out.entities;

import com.lights_out.dto.SolutionStepDTO;
import com.lights_out.entities.game.Point;

/**
 * An entity representing a solution step
 */
public class SolutionStep extends BaseEntity {
    private Long solutionFK;
    private Point clickedPoint;
    private Integer stepIndex;

    public SolutionStep(){}

    public SolutionStep(SolutionStepDTO dto) {
        this.solutionFK = dto.getSolutionFK();
        this.clickedPoint = dto.getClickPoint();
        this.stepIndex = dto.getStepIndex();
    }

    public SolutionStep(Long solutionFK, Point clickedPoint, Integer stepIndex) {
        this.solutionFK = solutionFK;
        this.clickedPoint = clickedPoint;
        this.stepIndex = stepIndex;
    }

    /**
     * Get the id of the associated solution
     *
     * @return the id of the associated solution
     */
    public Long getSolutionFK() {
        return solutionFK;
    }

    /**
     * Set the id of the associated solution
     *
     * @param solutionFK the id of the associated solution
     */
    public void setSolutionFK(Long solutionFK) {
        this.solutionFK = solutionFK;
    }

    /**
     * Get the point clicked in this step
     *
     * @return the clicked point
     */
    public Point getClickedPoint() {
        return clickedPoint;
    }

    /**
     * Set the point clicked in this step
     *
     * @param clickedPoint the clicked point
     */
    public void setClickedPoint(Point clickedPoint) {
        this.clickedPoint = clickedPoint;
    }

    /**
     * Get the order index at which this step is performed in the solution
     *
     * @return the order index
     */
    public Integer getStepIndex() {
        return stepIndex;
    }

    /**
     * Set the order index at which this step is performed in the solution
     *
     * @param stepIndex the order index
     */
    public void setStepIndex(Integer stepIndex) {
        this.stepIndex = stepIndex;
    }
}
