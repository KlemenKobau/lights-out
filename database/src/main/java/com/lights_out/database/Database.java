package com.lights_out.database;

import com.lights_out.dto.ProblemDTO;
import com.lights_out.dto.SolutionDTO;
import com.lights_out.dto.SolutionStepDTO;
import com.lights_out.entities.Problem;
import com.lights_out.entities.Solution;
import com.lights_out.entities.SolutionStep;
import com.lights_out.entities.game.Point;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.*;
import java.util.stream.Collectors;

/**
 * An in-memory database used by the application
 */
@Singleton
public class Database {

    private long problemDatabaseCounter;
    private long solutionDatabaseCounter;
    private long solutionStepDatabaseCounter;

    private final Map<Long, Problem> problemDatabase;
    private final Map<Long, Solution> solutionDatabase;
    private final Map<Long, SolutionStep> solutionStepDatabase;

    @Inject
    public Database(){
        this.problemDatabaseCounter = 0;
        this.solutionDatabaseCounter = 0;
        this.solutionStepDatabaseCounter = 0;

        this.problemDatabase = new HashMap<>();
        this.solutionDatabase = new HashMap<>();
        this.solutionStepDatabase = new HashMap<>();
    }

    // Problem

    /**
     * Save a problem to the database
     *
     * @param problemDTO a problem we wish to save
     * @return the saved problem
     */
    public Problem addProblem(ProblemDTO problemDTO) {
        Problem problem = new Problem(problemDTO);
        problem.setId(problemDatabaseCounter);
        problemDatabaseCounter += 1;
        problemDatabase.put(problem.getId(), problem);

        return problem;
    }

    /**
     * Get all saved problems
     *
     * @return a list of saved problems
     */
    public List<ProblemDTO> getAllProblems() {
        return problemDatabase.values().stream().map(Problem::toProblemDTO).collect(Collectors.toList());
    }

    /**
     * Get a problem by id
     *
     * @param id the id of the problem we are looking for
     * @return an optional containing the problem or an empty optional
     */
    public Optional<ProblemDTO> getProblemById(long id) {
        return Optional.ofNullable(problemDatabase.get(id).toProblemDTO());
    }

    // Solution

    /**
     * Save a solution to the database
     *
     * @param solutionDTO a solution we wish to save
     * @return the saved solution
     */
    public Solution addSolution(SolutionDTO solutionDTO) {
        Solution solution = new Solution(solutionDTO);
        solution.setId(solutionDatabaseCounter);
        solutionDatabaseCounter += 1;
        solutionDatabase.put(solution.getId(), solution);

        return solution;
    }

    /**
     * Get all solutions from the database
     *
     * @return a list of all solutions
     */
    public List<SolutionDTO> getAllSolutions() {
        return solutionsToSolutionDTOs(new LinkedList<>(solutionDatabase.values()));
    }

    /**
     * Get all solutions that correspond to a problem with id problemId
     *
     * @param problemId the problemId that has to be referenced by the solutions
     * @return a list of solutions
     */
    public List<SolutionDTO> getSolutionsByProblemId(long problemId) {
        List<Solution> solutions = solutionDatabase.values().stream()
                .filter(solution -> solution.getProblemFK() == problemId)
                .collect(Collectors.toList());

        return solutionsToSolutionDTOs(solutions);
    }

    /**
     * A transformation function between in database entities and dtos
     *
     * @param solutions a list of solution entities
     * @return a list of solution dto
     */
    private List<SolutionDTO> solutionsToSolutionDTOs(List<Solution> solutions) {
        List<SolutionDTO> solutionDTOS = new LinkedList<>();

        for (Solution solution : solutions) {
            SolutionDTO dto = new SolutionDTO();
            dto.setProblemFK(solution.getProblemFK());
            dto.setClickPoints(getPointClicksForSolutionById(solution.getId()));
            solutionDTOS.add(dto);
        }

        return solutionDTOS;
    }

    // Solution step

    /**
     * Save a solution step
     *
     * @param solutionStepDTO the solution step to be saved
     */
    public void addSolutionStep(SolutionStepDTO solutionStepDTO) {
        SolutionStep solutionStep = new SolutionStep(solutionStepDTO);
        solutionStep.setId(solutionStepDatabaseCounter);
        solutionStepDatabaseCounter += 1;
        solutionStepDatabase.put(solutionStep.getId(), solutionStep);
    }

    /**
     * Get points to click for in certain solution
     *
     * @param id the id of the solution in question
     * @return a list of points
     */
    public List<Point> getPointClicksForSolutionById(long id) {
        return solutionStepDatabase.values().stream()
                .filter(solutionStep -> solutionStep.getSolutionFK() == id)
                .sorted(Comparator.comparingLong(SolutionStep::getStepIndex))
                .map(SolutionStep::getClickedPoint)
                .collect(Collectors.toList());
    }
}
