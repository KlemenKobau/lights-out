package com.lights_out.services;

import com.lights_out.dto.ProblemDTO;
import com.lights_out.dto.SolutionDTO;

import java.util.Optional;

/**
 * Defines the functionality of a lights out solver bean
 */
public interface ProblemSolverBean {

    /**
     * Solve a lights out problem
     *
     * @param problemDTO A dto representation of the problem
     * @return A solution or an empty optional if a solution does not exist
     */
    Optional<SolutionDTO> solve(ProblemDTO problemDTO);
}
