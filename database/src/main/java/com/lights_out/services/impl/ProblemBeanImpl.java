package com.lights_out.services.impl;


import com.lights_out.database.Database;
import com.lights_out.dto.ProblemDTO;
import com.lights_out.dto.SolutionDTO;
import com.lights_out.dto.SolutionStepDTO;
import com.lights_out.entities.Problem;
import com.lights_out.entities.Solution;
import com.lights_out.entities.game.Point;
import com.lights_out.services.ProblemBean;
import com.lights_out.services.ProblemSolverBean;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.BadRequestException;
import java.util.List;
import java.util.ListIterator;
import java.util.Optional;

/**
 * An implementation of the ProblemBean interface
 */
@RequestScoped
public class ProblemBeanImpl implements ProblemBean {

    private final Database database;
    private final ProblemSolverBean problemSolverBean;

    @Inject
    public ProblemBeanImpl(Database database, ProblemSolverBean problemSolverBean) {
        this.database = database;
        this.problemSolverBean = problemSolverBean;
    }

    public List<ProblemDTO> getAllProblems() {
        return database.getAllProblems();
    }

    public Optional<ProblemDTO> getProblemById(long id) {
        return database.getProblemById(id);
    }

    public void saveAProblem(ProblemDTO problemDTO) {

        if (problemDTO.getInitialState().length > 8) {
            throw new BadRequestException("The problem must be 8x8 or smaller");
        } else if (problemDTO.getInitialState().length < 3) {
            throw new BadRequestException("The problem must be 3x3 or bigger");
        }

        // the problem has to be square
        for (boolean[] row : problemDTO.getInitialState()) {
            if (problemDTO.getInitialState().length != row.length) {
                throw new BadRequestException("Problem has to be square");
            }
        }

        Optional<SolutionDTO> solutionDTOOpt = problemSolverBean.solve(problemDTO);

        if (solutionDTOOpt.isEmpty()) {
            throw new BadRequestException("The provided problem has no solution");
        }

        SolutionDTO solutionDTO = solutionDTOOpt.get();

        Problem problem = database.addProblem(problemDTO);
        solutionDTO.setProblemFK(problem.getId());

        Solution solution = database.addSolution(solutionDTO);
        saveSolutionSteps(solutionDTO.getClickPoints(), solution.getId());
    }

    private void saveSolutionSteps(List<Point> points, long solutionId) {
        ListIterator<Point> iterator = points.listIterator();

        while (iterator.hasNext()) {
            int index = iterator.nextIndex();
            Point point = iterator.next();

            SolutionStepDTO dto = new SolutionStepDTO();
            dto.setSolutionFK(solutionId);
            dto.setClickPoint(point);
            dto.setStepIndex(index);

            database.addSolutionStep(dto);
        }
    }
}
