package com.lights_out.services.impl;

import com.lights_out.dto.ProblemDTO;
import com.lights_out.dto.SolutionDTO;
import com.lights_out.entities.game.Point;
import com.lights_out.services.ProblemSolverBean;
import org.jboss.logging.Logger;

import javax.enterprise.context.RequestScoped;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * An implementation of the lights out solver.
 * Solves a system of linear equations in Z_2 using Gaussian elimination.
 */
@RequestScoped
public class ProblemSolverBeanImpl implements ProblemSolverBean {

    private final Logger LOG = Logger.getLogger(ProblemSolverBeanImpl.class.getName());

    /**
     * Solve a square lights out problem
     *
     * @param problemDTO a dto representing the problem
     * @return a solution if one exists or an empty optional
     */
    public Optional<SolutionDTO> solve(ProblemDTO problemDTO) {

        LOG.info("Started looking for a solution");
        // in order to end up with only 1 in the matrix, the problem has to be inverted
        // the algorithm solves so that you end up with only zeros
        boolean[][] invertedProblem = invertProblem(problemDTO.getInitialState());

        // creating an augmented matrix associated to the initial problem
        // the matrix consists of a matrix representing relationships between fields when a certain field is clicked
        // and a vectorized initial problem
        boolean[][] augmentedMatrix = getAugmentedMatrixForProblem(invertedProblem);

        // The performance can be further improved by caching the solutions of Gaussian elimination
        // The problem then becomes O(n^2) with n being the length of a side in the initial problem grid
        // For small problems the current implementation is good enough
        toUpperTrapezoid(augmentedMatrix);
        Optional<List<Point>> solutionPointsOpt = getSolutionPoints(invertedProblem.length, augmentedMatrix);

        LOG.info("Finished looking for a solution");

        if (solutionPointsOpt.isEmpty()) {
            LOG.info("The problem has no solution");
            return Optional.empty();
        }

        LOG.info(String.format("The problem is solvable in %d steps", solutionPointsOpt.get().size()));
        // Setting the solution
        SolutionDTO solutionDTO = new SolutionDTO();
        solutionDTO.setClickPoints(solutionPointsOpt.get());

        return Optional.of(solutionDTO);
    }

    /**
     * Create a matrix for a system of linear equations to solve the lights out problem.
     * Only guaranteed to find a solution for square problems.
     *
     * @param initialState the lights out problem to be solved
     * @return the augmented matrix representing the problem
     */
    private boolean[][] getAugmentedMatrixForProblem(boolean[][] initialState) {

        // assume square problem
        int gridSide = initialState.length;

        // the matrix of field interactions with the last column being the vectorized initial state
        boolean[][] augmentedMatrix = new boolean[gridSide * gridSide][gridSide * gridSide + 1];

        // init to 0
        for (int i = 0; i < augmentedMatrix.length; i++) {
            for (int j = 0; j < augmentedMatrix[0].length; j++) {
                augmentedMatrix[i][j] = false;
            }
        }

        // diagonal has all ones
        for (int i = 0; i < gridSide * gridSide; i++) {
            augmentedMatrix[i][i] = true;
        }

        // two lines next to the center part have only ones
        for (int i = 0; i < gridSide * gridSide - gridSide; i++) {
            augmentedMatrix[i + gridSide][i] = true;
            augmentedMatrix[i][i + gridSide] = true;
        }

        // two off diagonals have a zero on every n-th step if n is one side of an array
        for (int i = 0; i < gridSide * gridSide - 1; i++) {
            if (i % gridSide != gridSide - 1) {
                augmentedMatrix[i + 1][i] = true;
                augmentedMatrix[i][i + 1] = true;
            }
        }

        // vectorize the initial matrix
        for (int i = 0; i < initialState.length; i++) {
            for (int j = 0; j < initialState[0].length; j++) {
                augmentedMatrix[i * gridSide + j][gridSide * gridSide] = initialState[i][j];
            }
        }

        return augmentedMatrix;
    }

    /**
     * Perform Gaussian elimination in Z_2 to create an upper trapezoid matrix.
     * The function may return 0 rows when the initial problem is 4x4 or bigger.
     * The operation is IN PLACE.
     *
     * @param augmentedMatrix the matrix to be transformed to upper trapezoid.
     */
    private void toUpperTrapezoid(boolean[][] augmentedMatrix) {
        int sideSize = augmentedMatrix.length;

        // Gaussian elimination in Z_2
        for (int i = 0; i < sideSize; i++) {
            LOG.debug(arrayToVisualString(augmentedMatrix));
            for (int j = i + 1; j < sideSize; j++) {
                if (augmentedMatrix[j][i]) {
                    arrayXOR(augmentedMatrix[i], augmentedMatrix[j]);
                }
            }

            // swapping rows so that we get an upper trapezoid matrix
            for (int j = i + 1; j < sideSize; j++) {
                if (augmentedMatrix[j][i + 1]) {
                    swapRows(augmentedMatrix, i + 1, j);
                    break;
                }
            }
        }
    }

    /**
     * Perform an in place XOR operation on two vectors.
     *
     * @param originalArray the array used for xor
     * @param arrayToModify the array to modify
     */
    private void arrayXOR(boolean[] originalArray, boolean[] arrayToModify) {
        // performs an XOR operation on 2 boolean arrays
        // I hope java vectorizes this, otherwise it could be even faster
        for (int i = 0; i < arrayToModify.length; i++) {
            arrayToModify[i] = arrayToModify[i] ^ originalArray[i]; // XOR operation
        }
    }

    /**
     * Swap two rows in a 2D matrix
     *
     * @param holderArray the 2D array holding the rows we wish to swap
     * @param firstIndex  the index of the first array
     * @param secondIndex the index of the second array
     */
    private void swapRows(boolean[][] holderArray, int firstIndex, int secondIndex) {
        boolean[] temp = holderArray[firstIndex];
        holderArray[firstIndex] = holderArray[secondIndex];
        holderArray[secondIndex] = temp;
    }

    /**
     * Solve a system of linear equations represented with an upper trapezoid matrix.
     *
     * @param initialProblemDim the side length of the initial lights out problem
     * @param upperTrapezoid    the upper trapezoid matrix representing a system of equations
     * @return a list of points representing a solution or an empty optional
     */
    private Optional<List<Point>> getSolutionPoints(int initialProblemDim, boolean[][] upperTrapezoid) {

        // checking if any row contains 0+0+...+0 = 1
        // this means that there is no solution
        for (boolean[] row : upperTrapezoid) {
            boolean allZeros = true;
            for (int j = 0; j < upperTrapezoid.length; j++) {
                if (row[j]) {
                    allZeros = false;
                    break;
                }
            }
            if (allZeros && row[row.length - 1]) {
                return Optional.empty();
            }
        }

        // holds the values for which lights to light up
        boolean[] values = new boolean[upperTrapezoid.length];

        // solve a system of linear equations
        // in cases where the final rows are 0+...+0=0
        // the bottom lights get assigned to 0, but they could also get assigned to 1 to get additional solutions
        // (taking the other restrictions into account)
        for (int i = upperTrapezoid.length - 1; i >= 0; i--) {
            boolean trueValue = upperTrapezoid[i][upperTrapezoid.length];

            for (int j = i + 1; j < upperTrapezoid.length; j++) {
                if (upperTrapezoid[i][j] && values[j]) {
                    trueValue = !trueValue;
                }
            }

            values[i] = trueValue;
        }

        // reshape the solution vector to get points in 2D
        List<Point> clickedPoints = new LinkedList<>();
        for (int i = 0; i < values.length; i++) {
            if (values[i]) {
                clickedPoints.add(new Point(i / initialProblemDim, i % initialProblemDim));
            }
        }

        return Optional.of(clickedPoints);
    }

    /**
     * negate all boolean values in a matrix
     *
     * @param originalProblem the matrix representing the original problem
     * @return a new array representing a negated matrix
     */
    private boolean[][] invertProblem(boolean[][] originalProblem) {
        boolean[][] invertedProblem = new boolean[originalProblem.length][originalProblem[0].length];

        for (int i = 0; i < originalProblem.length; i++) {
            for (int j = 0; j < originalProblem[0].length; j++) {
                invertedProblem[i][j] = !originalProblem[i][j];
            }
        }

        return invertedProblem;
    }

    /**
     * Get a string used for logging the matrices
     *
     * @param array the matrix to be transformed
     * @return a multiline visual representation of the matrix
     */
    private String arrayToVisualString(boolean[][] array) {
        return "\n" + Arrays.stream(array)
                .map(Arrays::toString)
                .collect(Collectors.joining(System.lineSeparator()));
    }
}
