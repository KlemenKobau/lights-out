package com.lights_out.services.impl;

import com.lights_out.database.Database;
import com.lights_out.dto.ProblemDTO;
import com.lights_out.dto.ProblemWithSolutionDTO;
import com.lights_out.dto.SolutionDTO;
import com.lights_out.services.ProblemBean;
import com.lights_out.services.SolutionBean;
import org.jboss.logging.Logger;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.BadRequestException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * An implementation of the SolutionBean interface
 */
@RequestScoped
public class SolutionBeanImpl implements SolutionBean {

    private final Logger LOG = Logger.getLogger(SolutionBeanImpl.class.getName());
    private final Database database;
    private final ProblemBean problemBean;

    @Inject
    public SolutionBeanImpl(Database database, ProblemBean problemBean) {
        this.database = database;
        this.problemBean = problemBean;
    }

    public List<ProblemWithSolutionDTO> getAllSolutions() {
        List<SolutionDTO> solutions = database.getAllSolutions();
        return mapToProblemWithSolution(solutions);
    }

    public List<SolutionDTO> getAllSolutionsForProblem(long id) {
        if (problemBean.getProblemById(id).isEmpty()) {
            throw new BadRequestException("No problem with provided id");
        }

        return database.getSolutionsByProblemId(id);
    }

    private List<ProblemWithSolutionDTO> mapToProblemWithSolution(List<SolutionDTO> solutions) {
        return solutions.stream().map(dto -> {
            ProblemWithSolutionDTO withSolutionDTO = new ProblemWithSolutionDTO();
            withSolutionDTO.setSolution(dto);

            Optional<ProblemDTO> problemDTOOpt = database.getProblemById(dto.getProblemFK());
            if (problemDTOOpt.isEmpty()) {
                LOG.error("No problem found for solution with foreign key: " + dto.getProblemFK());
                return null;
            }

            withSolutionDTO.setProblem(problemDTOOpt.get());
            return withSolutionDTO;
        }).filter(Objects::isNull).collect(Collectors.toList());
    }
}
