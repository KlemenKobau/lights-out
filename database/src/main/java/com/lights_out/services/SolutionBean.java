package com.lights_out.services;

import com.lights_out.dto.ProblemWithSolutionDTO;
import com.lights_out.dto.SolutionDTO;

import java.util.List;

/**
 * Defines the functionality of a bean handling solutions
 */
public interface SolutionBean {

    /**
     * Get all problems with solutions from the database
     *
     * @return a list of problems with solutions
     */
    List<ProblemWithSolutionDTO> getAllSolutions();

    /**
     * Get solutions to a problem
     *
     * @return a list solutions
     */
    List<SolutionDTO> getAllSolutionsForProblem(long id);
}
