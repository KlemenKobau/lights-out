package com.lights_out.services;

import com.lights_out.dto.ProblemDTO;

import java.util.List;
import java.util.Optional;

/**
 * Defines the functionality of a bean handling Problems
 */
public interface ProblemBean {

    /**
     * Save a problem to the database, if the problem has a solution or throws an exception otherwise.
     *
     * @param problemDTO a problem to be saved
     */
    void saveAProblem(ProblemDTO problemDTO);

    /**
     * Get all problems from the database
     *
     * @return a list of all problems
     */
    List<ProblemDTO> getAllProblems();

    /**
     * Get a problem by id
     *
     * @param id the id of the problem
     * @return the associated problem or an empty optional
     */
    Optional<ProblemDTO> getProblemById(long id);
}
