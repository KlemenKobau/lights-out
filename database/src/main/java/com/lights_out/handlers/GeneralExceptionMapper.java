package com.lights_out.handlers;

import org.jboss.logging.Logger;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * A class that catches general exceptions and reports a server error.
 * Catching to not leak the implementation of the application.
 */
@Provider
public class GeneralExceptionMapper implements ExceptionMapper<Exception> {

    private final Logger LOG = Logger.getLogger(GeneralExceptionMapper.class.getName());

    @Override
    public Response toResponse(Exception e) {
        LOG.error("Server error", e);
        Message message = new Message("Server error");
        return Response.serverError().entity(message).build();
    }
}
