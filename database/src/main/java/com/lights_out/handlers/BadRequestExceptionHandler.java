package com.lights_out.handlers;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * A class that handles Bad request exceptions
 */
@Provider
public class BadRequestExceptionHandler implements ExceptionMapper<BadRequestException> {
    @Override
    public Response toResponse(BadRequestException e) {
        Message message = new Message();
        if (e.getMessage() != null) {
            message.setMessage(e.getMessage());
        } else {
            message.setMessage("Bad request");
        }

        return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
    }
}
