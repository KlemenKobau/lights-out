package com.lights_out.handlers;

/**
 * A class used to send messages to the client in cases of errors.
 */
public class Message {
    private String message;

    public Message() {
    }

    public Message(String message) {
        this.message = message;
    }

    /**
     * Get the message associated to the error
     *
     * @return the error message
     */
    public String getMessage() {
        return message;
    }

    /**
     * Set the message associated with the error.
     *
     * @param message the error message
     */
    public void setMessage(String message) {
        this.message = message;
    }
}
