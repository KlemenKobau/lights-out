package com.lights_out.solver;

import com.lights_out.dto.ProblemDTO;
import com.lights_out.dto.SolutionDTO;
import com.lights_out.entities.game.Point;
import com.lights_out.services.ProblemSolverBean;
import io.quarkus.test.junit.QuarkusTest;
import org.jboss.logging.Logger;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

/**
 * Tests for the problem solver
 */
@QuarkusTest
public class SolverTests {

    private final Logger LOG = Logger.getLogger(SolverTests.class.getName());
    private final ProblemSolverBean problemSolverBean;

    @Inject
    public SolverTests(ProblemSolverBean problemSolverBean) {
        this.problemSolverBean = problemSolverBean;
    }

    /**
     * A 3x3 matrix with only ones, no lights
     */
    @Test
    void testWithNoLights() {
        boolean[][] onlyLights = new boolean[3][3];
        for (int i = 0; i < onlyLights.length; i++) {
            for (int j = 0; j < onlyLights.length; j++) {
                onlyLights[i][j] = true; // this corresponds to lights being off
            }
        }

        ProblemDTO problemDTO = new ProblemDTO();
        problemDTO.setId(0L);
        problemDTO.setInitialState(onlyLights);

        Optional<SolutionDTO> solutionDTOOpt = problemSolverBean.solve(problemDTO);
        Assertions.assertTrue(solutionDTOOpt.isPresent());

        SolutionDTO solutionDTO = solutionDTOOpt.get();
        Assertions.assertIterableEquals(List.of(), solutionDTO.getClickPoints());
    }

    /**
     * A 3x3 matrix with only zeros, all lights
     */
    @Test
    void testWithOnlyLights() {
        boolean[][] onlyLights = new boolean[3][3];
        for (int i = 0; i < onlyLights.length; i++) {
            for (int j = 0; j < onlyLights.length; j++) {
                onlyLights[i][j] = false; // this corresponds to lights being on
            }
        }

        ProblemDTO problemDTO = new ProblemDTO();
        problemDTO.setId(0L);
        problemDTO.setInitialState(onlyLights);

        Optional<SolutionDTO> solutionDTOOpt = problemSolverBean.solve(problemDTO);
        Assertions.assertTrue(solutionDTOOpt.isPresent());

        SolutionDTO solutionDTO = solutionDTOOpt.get();

        List<Point> solution = List.of(new Point(0, 0),
                new Point(0, 2),
                new Point(1, 1),
                new Point(2, 0),
                new Point(2, 2));
        solutionDTO.getClickPoints().sort(Comparator.comparing(Point::getyCoordinate)
                .thenComparing(Point::getxCoordinate));
        Assertions.assertIterableEquals(solution, solutionDTO.getClickPoints());
    }

    /**
     * A 4x4 matrix containing an unsolvable problem
     */
    @Test
    void problemWithNoSolution() {
        boolean[][] onlyLights = new boolean[4][4];
        for (int i = 0; i < onlyLights.length; i++) {
            for (int j = 0; j < onlyLights.length; j++) {
                onlyLights[i][j] = true;
            }
        }

        onlyLights[0][3] = false;
        onlyLights[1][1] = false;

        ProblemDTO problemDTO = new ProblemDTO();
        problemDTO.setId(0L);
        problemDTO.setInitialState(onlyLights);

        Optional<SolutionDTO> solutionDTOOpt = problemSolverBean.solve(problemDTO);
        Assertions.assertTrue(solutionDTOOpt.isEmpty());
    }

    /**
     * A 4x4 matrix containing a solvable problem
     */
    @Test
    void solvableFourByFour() {
        boolean[][] onlyLights = new boolean[4][4];
        for (int i = 0; i < onlyLights.length; i++) {
            for (int j = 0; j < onlyLights.length; j++) {
                onlyLights[i][j] = false;
            }
        }

        ProblemDTO problemDTO = new ProblemDTO();
        problemDTO.setId(0L);
        problemDTO.setInitialState(onlyLights);

        Optional<SolutionDTO> solutionDTOOpt = problemSolverBean.solve(problemDTO);
        Assertions.assertTrue(solutionDTOOpt.isPresent());

        SolutionDTO solutionDTO = solutionDTOOpt.get();

        List<Point> solution = List.of(new Point(0, 0),
                new Point(0, 1),
                new Point(0, 2),
                new Point(0, 3),
                new Point(1, 0),
                new Point(1, 3),
                new Point(2, 0),
                new Point(2, 1),
                new Point(2, 2),
                new Point(2, 3));
        solutionDTO.getClickPoints().sort(Comparator.comparing(Point::getyCoordinate)
                .thenComparing(Point::getxCoordinate));
        Assertions.assertIterableEquals(solution, solutionDTO.getClickPoints());
    }

    /**
     * A 20x20 matrix used for speed testing. Sutner (1989) showed that all lights problems are always solvable so
     * we use all lights.
     */
    @Test
    void bigProblemSpeedTest() {
        boolean[][] onlyLights = new boolean[20][20];
        for (int i = 0; i < onlyLights.length; i++) {
            for (int j = 0; j < onlyLights.length; j++) {
                onlyLights[i][j] = false;
            }
        }

        ProblemDTO problemDTO = new ProblemDTO();
        problemDTO.setId(0L);
        problemDTO.setInitialState(onlyLights);

        LOG.info("Started solving the big problem");
        Optional<SolutionDTO> solutionDTOOpt = problemSolverBean.solve(problemDTO);
        LOG.info("Finished solving the big problem");
        Assertions.assertTrue(solutionDTOOpt.isPresent());
    }
}
